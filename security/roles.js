/**
 * This module define authority roles
 * @module authorityRoles
 */

let authorityRoles = {
	ADMIN: 'System Admin',
	FIELD_AGENT: 'Field Agent',
	CONTROL_CENTER_AGENT: 'Control Centre Agent',
};

/** export authority roles */
module.exports = authorityRoles;
